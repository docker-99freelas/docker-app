FROM node:16.15.1

RUN mkdir -p /main && npm install -g ionic@5.2.0

WORKDIR /main

EXPOSE 8100