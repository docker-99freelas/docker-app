FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
RUN echo "debconf shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections && \
    echo "debconf shared/accepted-oracle-license-v1-1 seen true" | debconf-set-selections

ENV SDK_URL="https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip" \
    ANDROID_HOME="/usr/local/android-sdk" \
    ANDROID_VERSION="29" \
    ANDROID_BUILD_TOOLS_VERSION="33.0.0" \
    CORDOVA_VERSION="9.0.0" \
    IONIC_VERSION="5.2.0" \
    NODE_VERSION="18.12.1" \
    GRADLE_VERSION="6.9.3"
ENV GRADLE_URL="https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip" \
    GRADLE_HOME="/opt/gradle/gradle-${GRADLE_VERSION}" \
    PATH="/opt/gradle/gradle-${GRADLE_VERSION}/bin:${PATH}:${ANDROID_HOME}/cmdline-tools/tools:${ANDROID_HOME}/cmdline-tools/tools/bin:${ANDROID_HOME}/platform-tools"

COPY android/repositories.cfg /root/.android/

# Download JDK-8 and fix certificate issues
RUN apt-get update \
 && apt-get install -y openjdk-8-jdk \
 && apt-get install -y ant \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /var/cache/oracle-jdk8-installer \
 && apt-get update \
 && apt-get install -y ca-certificates-java \
 && apt-get clean \
 && update-ca-certificates -f \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /var/cache/oracle-jdk8-installer

# Download Android SDK
RUN cd /tmp \
 && apt-get update \
 && apt-get install -y nano zip curl net-tools socat \
 && curl -o gradle.zip -L "$GRADLE_URL" \
 && unzip -d /opt/gradle gradle.zip \
 && rm gradle.zip \
 && mkdir -p "$ANDROID_HOME/cmdline-tools" \
 && cd "$ANDROID_HOME/cmdline-tools" \
 && curl -o sdk.zip $SDK_URL \
 && unzip sdk.zip \
 && rm sdk.zip \
 && yes | "$ANDROID_HOME/cmdline-tools/tools/bin/sdkmanager" --licenses \
 && "$ANDROID_HOME/cmdline-tools/tools/bin/sdkmanager" --update \
 && "$ANDROID_HOME/cmdline-tools/tools/bin/sdkmanager" \
    "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
    "platforms;android-${ANDROID_VERSION}" \
    "platform-tools" \
    "emulator" \
 && curl -sL "https://deb.nodesource.com/setup_${NODE_VERSION}.x" | bash - \
 && apt-get update \
 && apt-get install -y nodejs \
 && npm install -g cordova@$CORDOVA_VERSION \
 && npm install -g ionic@$IONIC_VERSION \
 && node -v && npm -v

RUN mkdir /main
WORKDIR /main