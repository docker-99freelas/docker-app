FROM amazoncorretto:17.0.8

COPY android/repositories.cfg /root/.android/

ENV SDK_URL="https://dl.google.com/android/repository/commandlinetools-linux-10406996_latest.zip" \
    ANDROID_HOME="/usr/local/android-sdk" \
    ANDROID_VERSION="33" \
    ANDROID_BUILD_TOOLS_VERSION="33.0.2" \
    CORDOVA_VERSION="12.0.0" \
    NODE_VERSION="16" \
    GRADLE_VERSION="7.6"
ENV GRADLE_URL="https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip" \
    GRADLE_HOME="/opt/gradle/gradle-${GRADLE_VERSION}" \
    PATH="/opt/gradle/gradle-${GRADLE_VERSION}/bin:${PATH}:${ANDROID_HOME}/cmdline-tools/tools:${ANDROID_HOME}/cmdline-tools/tools/bin"

# Download Android SDK
RUN cd /tmp \
 && yum install -y curl nano zip unzip \
 && curl -o gradle.zip -L "$GRADLE_URL" \
 && unzip -d /opt/gradle gradle.zip \
 && rm gradle.zip \
 && mkdir -p "$ANDROID_HOME/cmdline-tools" \
 && cd "$ANDROID_HOME/cmdline-tools" \
 && curl -o sdk.zip $SDK_URL \
 && unzip sdk.zip \
 && rm sdk.zip \
 && mv "$ANDROID_HOME/cmdline-tools/cmdline-tools" "$ANDROID_HOME/cmdline-tools/tools" \
 && yes | "$ANDROID_HOME/cmdline-tools/tools/bin/sdkmanager" --licenses \
 && "$ANDROID_HOME/cmdline-tools/tools/bin/sdkmanager" --update \
 && "$ANDROID_HOME/cmdline-tools/tools/bin/sdkmanager" \
    "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
    "platforms;android-${ANDROID_VERSION}" \
    "platform-tools" \
 && curl -sL "https://rpm.nodesource.com/setup_${NODE_VERSION}.x" | bash - \
 && yum install -y nodejs \
 && npm install -g "cordova@$CORDOVA_VERSION" \
 && node -v && npm -v \
 && yum clean all

RUN mkdir /main
WORKDIR /main